import { createBrowserRouter, RouterProvider } from "react-router-dom";
import RootLayout from "./pages/user/RootLayout";
import PageLayout from "./pages/user/PageLayout";
const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    children: [{ path: "/", element: <PageLayout /> }],
  },
]);
function App() {
  return <RouterProvider router={router}></RouterProvider>;
}

export default App;
