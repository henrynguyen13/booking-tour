import * as React from "react";
import { Fragment } from "react";
// import Header from "../Layout/Header/Header";
// import Footer from "../Layout/Footer/Footer";
import { Outlet } from "react-router-dom";
const RootLayout = () => {
  return (
    <Fragment>
      {/* <Header /> */}
      <Outlet />
      {/* <Footer /> */}
    </Fragment>
  );
};
export default RootLayout;
