import * as React from "react";
import { Fragment } from "react";
import Cart from "../../components/cart/Cart";
const PageLayout = () => {
  return (
    <Fragment>
      <Cart />
    </Fragment>
  );
};
export default PageLayout;
